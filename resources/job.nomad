job "[[ .job_name ]]" {
    datacenters = "[[.datacenter]]"
    type = "service"
    update {
        max_parallel      = "[[.update.max_parallel]]"
        health_check      = "[[.update.health_check]]"
        min_healthy_time  = "[[.update.min_healthy_time]]"
        healthy_deadline  = "[[.update.healthy_deadline]]"
        progress_deadline = "[[.update.progress_deadline]]"
        auto_revert       = "[[.update.auto_revert]]"
        stagger           = "[[.update.stagger]]"
    }
    task "[[.job_name]]" {
        driver = "docker"
        service {
            name = "[[ .job_name ]]"
        }
        config {
            image = "lordchou/[[.application.image]]:[[.application.tag]]"
        }
        env {
            TOKEN="[[ env "BOT_TOKEN" ]]"
        }
        resources {
          cpu    = "[[.resources.cpu]]"
          memory = "[[.resources.memory]]"
        }
    }
}